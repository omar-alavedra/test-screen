# PROJECT Test-screen
by Omar Alavedra.

## Brief introduction
This project is a React-Native application screen based on a frontend code assessment. 
Is made with React-native using a framework called Expo and the reason why is because of the great efficiency, the high performance, 
Expo is a set of tools and services built around React Native and native platforms that help you develop, build, deploy, and quickly 
iterate on iOS, Android, and web apps from the same JavaScript/TypeScript codebase.
The main duty of this application screen is to expose the following services with their inherent restrictions:
*   Offer an adaptative screen for different device resoultions and for iOS and Android.
*   Follow the guidelines of a good UI and UX.
*   Make the design don't obstruct the functionality of the screen.
*   Make an intuitive screen, easy to use.

I hope the you enjoy interacting with test-screen, I accept all kinds of suggestions for improvement. Thank you very much for the opportunity.

## Improvements for the future
Based on the time of the development (One day), there are some improvements to do in the near future like:
* Improve the structure of the code.
* Add more testing.
* Find bugs or improvements to be tested with time and solved.


## Requirements

For development, you will need Npm, Node.js, Expo and Git installed on your environement.

#### Node and Expo installation on OS X

You will need to use a Terminal. On OS X, you can find the default terminal in
`/Applications/Utilities/Terminal.app`.

Please install [Homebrew](http://brew.sh/) if it's not already done with the following command.

    $ ruby -e "$(curl -fsSL https://raw.github.com/Homebrew/homebrew/go/install)"

If everything when fine, you should run

    brew install node
    npm install -g expo-cli
    
#### Node and Expo installation on Linux

    sudo apt-get install python-software-properties
    sudo add-apt-repository ppa:chris-lea/node.js
    sudo apt-get update
    sudo apt-get install nodejs
    npm install -g expo-cli

#### Node installation on Windows

Just go on [official Node.js website](http://nodejs.org/) & grab the installer.
Also, be sure to have `git` available in your PATH, `npm` might need it.

---

## Install

    $ git clone https://gitlab.com/omar-alavedra/test-screen.git
    $ cd test-screen
    $ npm install

## Start & watch (development)
    
    1) Open the Android Studio and start a Virtual Device, Open Xcode and start a simulated Device.
    
    2) Open a terminal and run:

        $ npm start
        
    3) Go to the initialized local-webpage (expo) and click:
        
        - Run on Android device
        - Run on iOS device.

    
## Languages & tools

### JavaScript

- [expo](https://docs.expo.io/versions/v35.0.0/)  is a set of tools and services built around React Native and native platforms that help you develop, build, deploy, and quickly iterate on iOS, Android, and web apps from the same JavaScript/TypeScript codebase.



