import React from 'react';
import * as Font from 'expo-font';
import { Text, View, TouchableOpacity, ActivityIndicator } from 'react-native';

import Header from '../components/header';
import styles from '../styles/styles'

export default class ExchangeScreen extends React.Component {
  constructor(){
    super()
    this.state = {
      fontLoaded: false,
      decimals: false,
      decimalOne: false,
      decimalTwo: false,
      balance: "1.117,82",
      amount: "0.00",
      maxLenght: "9999999.99"
    }
    this.activeDecimals = this.activeDecimals.bind(this);
    this.numberPress = this.numberPress.bind(this);

  }
  async componentDidMount() {
    await Font.loadAsync({
      'Avenir-Bold': require('../../assets/fonts/Avenir-Heavy.ttf'),
      'Avenir-Light': require('../../assets/fonts/Avenir-Light.ttf'),
      'Avenir-Medium': require('../../assets/fonts/Avenir-Medium.ttf'),
    });

    this.setState({
      fontLoaded: true
    })
  }

  activeDecimals () {
    if(!this.state.decimals){
      this.setState({
        decimals: true
      })
    }
  }

  erase () {
    var wholeNum = parseInt(this.state.amount).toString()
    var decimalsValue = parseFloat(this.state.amount).toFixed(2).toString().split(".")[1]
    var result
    if(this.state.decimals){
      if(this.state.decimalTwo){
        result = wholeNum + "." + decimalsValue[0]
        this.setState({
          decimalTwo: false,
          amount: result
        })
      }
      else if(this.state.decimalOne){
        this.setState({
          decimalOne: false,
          amount: wholeNum
        })
      }
      else if(this.state.decimals){
        this.setState({
          decimals: false
        })
      }
    }
    else{
      if(wholeNum != "0"){
        if(wholeNum.length -1 > 0){
          result = wholeNum.substring(0, wholeNum.length - 1);
          
        }
        else {
          result = "0"
        }
        this.setState({
          amount: result
        })
      }
      
    }
  }

  numberPress (number) {
    var wholeNum = parseInt(this.state.amount).toString()
    var decimalNum = parseFloat(this.state.amount).toFixed(2).toString().split(".")[1]
    if(!this.state.decimals){
      if(wholeNum > "0"){
        wholeNum += number
        if(parseFloat(wholeNum) < parseFloat(this.state.maxLenght)){
          this.setState({
            amount: wholeNum
          })
        }
       
      }
      else if (wholeNum == "0" && number != "0"){
        wholeNum = number
        this.setState({
          amount: wholeNum
        })
      }
    }
    else{
      var result
      if(!this.state.decimalOne){
        result = wholeNum + "." + number
        this.setState({
          decimalOne: true,
          amount: result
        })
      }
      else if(this.state.decimalOne){
        result = wholeNum + "." + decimalNum[0] + number
        this.setState({
          decimalTwo: true,
          amount: result
        })
      }
    }
  }

  render(){
    return (
      <View style={styles.container}>
        <Header/>
        {this.state.fontLoaded?
        <View style={styles.displayAmountContiner}>
          {parseInt(this.state.amount) < 1000?
          <View style={styles.displayAmountContent}>
              <Text adjustsFontSizeToFit style={styles.displayCurrencySign1}>€</Text>
              <Text style={styles.displayAmountText1} numberOfLines={1} adjustsFontSizeToFit>{parseInt(this.state.amount).toString().replace(/(.)(?=(.{3})+$)/g,"$1.")}</Text>
              {this.state.decimals?
              <View style={styles.displayDecimals}>
                <Text adjustsFontSizeToFit style={this.state.decimalOne?styles.displayDecimalActive1:styles.displayDecimalInactive1}>{parseFloat(this.state.amount).toFixed(2).toString().split(".")[1][0]}</Text>
                <Text adjustsFontSizeToFit style={this.state.decimalTwo?styles.displayDecimalActive1:styles.displayDecimalInactive1}>{parseFloat(this.state.amount).toFixed(2).toString().split(".")[1][1]}</Text>
              </View>:null}
          </View>
          :
          <View style={styles.displayAmountContent}>
              <Text adjustsFontSizeToFit style={parseInt(this.state.amount) < 100000?styles.displayCurrencySign2:styles.displayCurrencySign3}>€</Text>
              <Text style={parseInt(this.state.amount) < 100000?styles.displayAmountText2:styles.displayAmountText3} numberOfLines={1} adjustsFontSizeToFit>{parseInt(this.state.amount).toString().replace(/(.)(?=(.{3})+$)/g,"$1.")}</Text>
              {this.state.decimals?
                <View style={styles.displayDecimals}>
                  {parseInt(this.state.amount) < 100000?
                    <View style={styles.displayDecimals}>
                      <Text adjustsFontSizeToFit style={this.state.decimalOne?styles.displayDecimalActive2:styles.displayDecimalInactive2}>{parseFloat(this.state.amount).toFixed(2).toString().split(".")[1][0]}</Text>
                      <Text adjustsFontSizeToFit style={this.state.decimalTwo?styles.displayDecimalActive2:styles.displayDecimalInactive2}>{parseFloat(this.state.amount).toFixed(2).toString().split(".")[1][1]}</Text>
                    </View>
                    :
                    <View style={styles.displayDecimals}>
                      <Text adjustsFontSizeToFit style={this.state.decimalOne?styles.displayDecimalActive3:styles.displayDecimalInactive3}>{parseFloat(this.state.amount).toFixed(2).toString().split(".")[1][0]}</Text>
                      <Text adjustsFontSizeToFit style={this.state.decimalTwo?styles.displayDecimalActive3:styles.displayDecimalInactive3}>{parseFloat(this.state.amount).toFixed(2).toString().split(".")[1][1]}</Text>
                    </View>}
                </View>:null}
              
          </View>
          }
        </View>:<ActivityIndicator size='large'/>}
        {this.state.fontLoaded?
        <View style={styles.numPadContainer}>
          <View style={styles.row}>
                <TouchableOpacity onPress={() => this.numberPress("1")} style={styles.buttonLeft}>
                  <Text style={styles.textButton}>
                    1
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.numberPress("2")} style={styles.buttonCenter}>
                  <Text style={styles.textButton}>
                    2
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.numberPress("3")} style={styles.buttonRight}>
                  <Text style={styles.textButton}>
                    3
                  </Text>
                </TouchableOpacity>
          </View>
          <View style={styles.row}>
                <TouchableOpacity onPress={() => this.numberPress("4")} style={styles.buttonLeft}>
                  <Text style={styles.textButton}>
                    4
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.numberPress("5")} style={styles.buttonCenter}>
                  <Text style={styles.textButton}>
                    5
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.numberPress("6")} style={styles.buttonRight}>
                  <Text style={styles.textButton}>
                    6
                  </Text>
                </TouchableOpacity>
          </View>
          <View style={styles.row}>
                <TouchableOpacity onPress={() => this.numberPress("7")} style={styles.buttonLeft}>
                  <Text style={styles.textButton}>
                    7
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.numberPress("8")} style={styles.buttonCenter}>
                  <Text style={styles.textButton}>
                    8
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.numberPress("9")} style={styles.buttonRight}>
                  <Text style={styles.textButton}>
                    9
                  </Text>
                </TouchableOpacity>
          </View>
          <View style={styles.row}>
                <TouchableOpacity onPress={() => this.activeDecimals()} style={styles.buttonLeft}>
                  <Text style={styles.textButtonSign}>
                    {","}
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.numberPress("0")} style={styles.buttonCenter}>
                  <Text style={styles.textButton}>
                    0
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.erase()} style={styles.buttonRight}>
                  <Text style={styles.textButton}>
                    {"<"}
                  </Text>
                </TouchableOpacity>
          </View>
        </View>:<ActivityIndicator size='large'/>}
        {this.state.fontLoaded?<View style={styles.buttonsContainer}>
          <TouchableOpacity /*onPress={}*/ style={styles.functionalButton}>
            <Text style={styles.functionalTextButton}>
              Solicitar
            </Text>
          </TouchableOpacity>
          <TouchableOpacity /*onPress={}*/ style={styles.functionalButton}>
            <Text style={styles.functionalTextButton}>
              Enviar
            </Text>
          </TouchableOpacity>
        </View>:<ActivityIndicator size='large'/>}
      </View>
    );
  }
  
}

