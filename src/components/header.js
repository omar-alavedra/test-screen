import React from 'react';
import * as Font from 'expo-font';
import { StyleSheet, Text, View, Image, TouchableOpacity, ActivityIndicator, Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');
import ScalableText from 'react-native-text';

export default class Header extends React.Component {
  constructor(){
    super()
    this.state = {
      fontLoaded: false,
      decimals: false,
      decimalOne: false,
      decimalTwo: false,
      balance: "1.117,82",
      amount: "0.00",
      maxLenght: "9999999.99"
    }

  }
  async componentDidMount() {
    // console.log(Dimensions.get("window"))
    await Font.loadAsync({
      'Avenir-Bold': require('../../assets/fonts/Avenir-Heavy.ttf'),
      'Avenir-Light': require('../../assets/fonts/Avenir-Light.ttf'),
      'Avenir-Medium': require('../../assets/fonts/Avenir-Medium.ttf'),
    });

    this.setState({
      fontLoaded: true
    })
  }


  render(){
    return (
            
        <View style={styles.headerContainer}>
                {this.state.fontLoaded?
            <View style={styles.headerContainer}>
            <View style={styles.leftIconContainer}>
                <Image
                style={styles.leftIcon}
                resizeMode="contain"
                source={require('../../assets/images/userPicture.png')}
                />         
            </View> 
            <View style={styles.amountContainer}>
                <Text 
                style={styles.amountDisplayTitle}
                >Fondos</Text>
                <Text 
                style={styles.amountDisplay}
                >{this.state.balance} €
                </Text>
            </View>
            <Image
                style={styles.rightIcon}
                resizeMode="contain"
                source={require('../../assets/images/movimiento.png')}
            />
            </View>
            :<ActivityIndicator size='large'/>}
        </View>
    );
  }
  
}

const styles = StyleSheet.create({
  headerContainer:{
    paddingLeft: '2%',
    paddingRight: '2%',
    width: '100%',
    height: '8%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    position: 'relative'
  },
  amountContainer:{
    marginTop: '4%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'center',
    height: 25
  },
  amountDisplayTitle:{
    lineHeight: 23,
    letterSpacing: 2,
    fontFamily: 'Avenir-Bold',
    fontSize: 18,
  },
  amountDisplay:{
    lineHeight: 18,
    color: '#7d888f',
    fontFamily: 'Avenir-Medium',
    fontSize: 12,
  },
  leftIconContainer:{
    borderRadius: 100,
    borderColor: '#979797',
    borderWidth: 1.2,
    overflow: 'hidden'
  },
  leftIcon:{
    height: 35,
    width: 35,
  },
  rightIcon:{
    height: 25,
    width: 25
  },
});
