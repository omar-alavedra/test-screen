import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
      paddingLeft: '6%',
      paddingRight: '6%',
      paddingTop: '11%',
      justifyContent: 'space-between'
    },
    
    displayAmountContiner: {
      paddingRight: '8%',
      paddingLeft: '4%',
      marginLeft: 8,
      marginRight: 8,
      width: '100%',
      height: '30%',
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
    },
    displayAmountContent: {
      flexDirection: 'row',
      alignItems: 'flex-start',
      justifyContent: 'flex-start'
    },
    displayCurrencySign1: {
      marginRight: '4%',
      color: '#bed0d4',
      fontSize: 78,
      lineHeight: 86,
      fontFamily: 'Avenir-Bold',
    },
    displayCurrencySign2: {
      marginRight: '2%',
      color: '#bed0d4',
      fontSize: 54,
      lineHeight: 65,
      fontFamily: 'Avenir-Bold',
    },
    displayCurrencySign3: {
      marginRight: '2%',
      color: '#bed0d4',
      fontSize: 34,
      lineHeight: 40,
      fontFamily: 'Avenir-Bold',
    },
    
    displayAmountText1: {
      marginTop: '0%',
      marginRight: '.5%',
      lineHeight: 128,
      fontSize: 116,
      fontFamily: 'Avenir-Light',
    },
    displayAmountText2: {
      marginTop: '0%',
      marginRight: '.5%',
      lineHeight: 96,
      fontSize: 86,
      fontFamily: 'Avenir-Light',
    },
    displayAmountText3: {
      marginTop: '0%',
      marginRight: '.5%',
      lineHeight: 60,
      fontSize: 55,
      fontFamily: 'Avenir-Light',
    },
    displayDecimals:{
      display: 'flex',
      flexDirection: 'row'
    },
    displayDecimalInactive1:{
      color: '#bed0d4',
      lineHeight: 65,
      fontSize: 58,
      fontFamily: 'Avenir-Light',
    },
    displayDecimalActive1:{
      lineHeight: 65,
      fontSize: 58,
      fontFamily: 'Avenir-Light',
    },
  
    displayDecimalInactive2:{
      color: '#bed0d4',
      lineHeight: 47,
      paddingTop: 0,
      fontSize: 42,
      fontFamily: 'Avenir-Light',
    },
    displayDecimalActive2:{
      lineHeight: 47,
      paddingTop: 0,
      fontSize: 42,
      fontFamily: 'Avenir-Light',
    },
  
    displayDecimalInactive3:{
      color: '#bed0d4',
      lineHeight: 38,
      paddingTop: 0,
      fontSize: 32,
      fontFamily: 'Avenir-Light',
    },
    displayDecimalActive3:{
      lineHeight: 38,
      paddingTop: 0,
      fontSize: 32,
      fontFamily: 'Avenir-Light',
    },
    numPadContainer: {
      marginBottom: '8%',
      height: '45%',
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'space-between'
  
    },
    row: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    buttonLeft: {
      paddingBottom: '1%',
      borderBottomColor: '#bed0d4',
      borderBottomWidth: 1,
      borderRightColor:'#bed0d4',
      borderRightWidth: 1,
      width: '23%',
    },
    buttonCenter: {
      width: '25%',
      paddingBottom: '1%',
      borderBottomColor: '#bed0d4',
      borderBottomWidth: 1,
    },
    buttonRight: {
      width: '23%',
      paddingBottom: '1%',
      borderBottomColor: '#bed0d4',
      borderBottomWidth: 1,
      borderLeftColor:'#bed0d4',
      borderLeftWidth: 1,
    },
    textButtonSign: {
      textAlign: 'center',
      fontSize: 28,
      lineHeight: 28,
      fontFamily: 'Avenir-Light',
    },
    textButton: {
      textAlign: 'center',
      fontSize: 28,
      lineHeight: 34,
      fontFamily: 'Avenir-Light',
    },
    buttonsContainer: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between'
    },
    functionalButton: {
      width: '48%',
      paddingLeft: '11.3%',
      paddingRight: '11.3%',
      paddingBottom: '4.2%',
      paddingTop: '4.2%',
      backgroundColor: '#8e9ccc',
      borderRadius: 6,
    },
    functionalTextButton: {
      fontFamily: 'Avenir-Medium',
      fontSize: 16,
      color: 'white',
      textAlign: 'center',
  
    }
  });